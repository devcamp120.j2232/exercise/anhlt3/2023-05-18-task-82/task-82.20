package com.devcamp.customerbarchart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerBarChartApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerBarChartApplication.class, args);
	}

}
